import os
import sys
import time
import logging
import datetime

import sqlite3
import feedparser
from telegram import ParseMode, constants
from telegram.ext import Updater, CommandHandler
from telegram.error import TelegramError, RetryAfter

# Constants
LOG_CHANNEL_ID = 0  # Telegram Chat ID to write Logs into

TOKEN = ""  # Telegram API Token
PORT = int(os.environ.get('PORT', '6000'))  # Port for Webhook Server
HOST = "0.0.0.0"  # IP the Webhook Server binds to
WEBHOOK_URL = "https://example.org/" + TOKEN  # External Webhook URL

DBFILE = "bot.db"  # SQLite Filename

FEED_URL = "https://shw-clan.de/feed"  # RSS Feed to fetch entries from
FEED_CATEGORY_FILTER = ["Allgemein"]  # List of Categories to filter the feed

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

updater = Updater(TOKEN)
dispatcher = updater.dispatcher

# Init DB
conn = sqlite3.connect(DBFILE)
c = conn.cursor()
c.execute("""CREATE TABLE IF NOT EXISTS chats
              (chat_id integer,
               name text,
               subscriptions integer,
               last_update timestamp,
               UNIQUE(chat_id))""")
conn.commit()
conn.close()


def register_chat_to_db(func):
    """ Add the current Chat to the Database
    """
    def func_wrapper(update, context):
        connection = sqlite3.connect(DBFILE)
        cursor = connection.cursor()
        if update.message.chat.type == "private":
            cursor.execute(
              "INSERT OR IGNORE INTO chats VALUES (?, ?, 0, DateTime('now'))",
              (update.message.chat_id, update.message.chat.username)
            )
        elif hasattr(update.message, "title"):
            cursor.execute(
              "INSERT OR IGNORE INTO chats VALUES (?, ?, 0, DateTime('now'))",
              (update.message.chat_id, update.message.title)
            )
        connection.commit()
        connection.close()
        return func(update, context)
    return func_wrapper


@register_chat_to_db
def start(update, context):
    context.bot.sendMessage(
        chat_id=update.message.chat_id,
        text="Ich wurde von @GrandPhil entwickelt. Nutzung auf eigene Gefahr!"
    )
    context.bot.sendMessage(
        chat_id=update.message.chat_id,
        text="Gib /news ein, um SHW-News für diesen Chat zu aktivieren."
    )


def error(update, context):
    context.bot.sendMessage(chat_id=LOG_CHANNEL_ID,
                            text='Update "{update}" caused error "{error}"'
                            .format(update=update, error=context.error))
    logging.warning('Update "%s" caused error "%s"' % (update, context.error))


def check_subscriptions():
    """ Check the FEED_URL for new entries. Sends a message to each subscriber.
    """
    connection = sqlite3.connect(DBFILE, detect_types=sqlite3.PARSE_DECLTYPES |
                                 sqlite3.PARSE_COLNAMES)
    cursor = connection.cursor()

    feed = feedparser.parse(FEED_URL)

    for entry in reversed(feed.entries):
        published_datetime = datetime.datetime(*entry.published_parsed[:6])

        cursor.execute("SELECT chat_id, subscriptions, last_update FROM chats")

        for chat in cursor:
            chat_id = chat[0]
            subscriptions = chat[1]
            last_update = chat[2]

            # News
            if bin(subscriptions)[-1] == "1" and \
               entry.category in FEED_CATEGORY_FILTER and \
               published_datetime >= last_update:
                print("Sending News to Chat {chat_id}".format(chat_id=chat_id))
                while True:
                    try:
                        post_subscription_entry(entry, chat_id)
                        break
                    except RetryAfter as e:
                        # We are getting rate-limited by telegram
                        print("Waiting {seconds} Seconds before sending next messages...".format(seconds=e.retry_after))
                        time.sleep(e.retry_after + 5)  # Couple extra seconds


    # last_update = now
    connection.cursor().execute(
        "UPDATE chats SET last_update = DateTime('now')")
    connection.commit()
    connection.close()


def post_subscription_entry(entry, chat_id):
    """ Read RSS entry and format a message accordingly.
    """
    published_datetime = datetime.datetime(*entry.published_parsed[:6])

    entry_text = "<b>{title}</b>\n{description}\n<a>{link}</a>"
    entry_text = entry_text.format(
        title=entry.title,
        description=entry.description,
        link=entry.link
    )

    updater.bot.sendMessage(
        chat_id=chat_id,
        text=entry_text,
        parse_mode=ParseMode.HTML
    )


def user_is_permitted(chat, user):
    """ Returns true for private chats and group administrators
    """
    if chat.type == "private":
        return True
    try:
        chatmember_status = updater.bot.getChatMember(chat.id, user.id).status
        if chatmember_status == constants.CHATMEMBER_ADMINISTRATOR or \
           chatmember_status == constants.CHATMEMBER_CREATOR:
            return True
    except TelegramError:  # No admin in Group
        return False
    return False


@register_chat_to_db
def subscribe_news(update, context):
    """ Toggle News subscription for current chat.
    """
    if user_is_permitted(update.message.chat, update.message.from_user):

        connection = sqlite3.connect(DBFILE)
        cursor = connection.cursor()
        cursor.execute("SELECT subscriptions FROM chats WHERE chat_id = ?",
                       (update.message.chat_id,))
        chat = cursor.fetchone()
        subscriptions = chat[0]

        # Subscription status is saved in binary which is then converted to an
        #  integer. This allows us to dynamically increase the amount of
        #  possible subscriptions without touching the db model.
        # E.g.: subscriptions = 11 --> '0b1011'
        #  subscribed to 1, 2 and 4. Not subscribed to 3.
        if bin(subscriptions)[-1] == "1":
            subscriptions = int(bin(subscriptions)[:-1] + "0", base=0)
            context.bot.sendMessage(
                chat_id=update.message.chat_id,
                text="❌ Du erhälst keine News."
            )

        elif bin(subscriptions)[-1] == "0":
            subscriptions = int(bin(subscriptions)[:-1] + "1", base=0)
            context.bot.sendMessage(
                chat_id=update.message.chat_id,
                text="✔ Du erhälst nun News."
            )

        connection.cursor().execute(
            "UPDATE chats SET subscriptions = ? WHERE chat_id = ?",
            (subscriptions, update.message.chat_id)
        )
        connection.commit()
        connection.close()
    else:
        context.bot.sendMessage(
            chat_id=update.message.chat_id,
            text="🚫 Nur Gruppen-Admins können Abonnements verwalten."
        )


def update_chats():
    """ Update all chat names from telegram.
    """
    connection = sqlite3.connect(DBFILE)
    cursor = connection.cursor()
    cursor.execute("SELECT chat_id FROM chats")

    for chat_id in cursor:
        new_chat = updater.bot.get_chat(chat_id[0])

        if new_chat.type == "private":
            connection.cursor().execute(
                "UPDATE chats SET name = ? WHERE chat_id = ?",
                (new_chat.username, chat_id[0])
            )
        else:
            connection.cursor().execute(
                "UPDATE chats SET name = ? WHERE chat_id = ?",
                (new_chat.title, chat_id[0])
            )
        connection.commit()
    connection.close()


# Handler
dispatcher.add_handler(CommandHandler('start', start))
dispatcher.add_handler(CommandHandler('news', subscribe_news))
dispatcher.add_error_handler(error)


if __name__ == "__main__":
    updater.start_webhook(
        listen=HOST,
        port=PORT,
        url_path=TOKEN,
        webhook_url=WEBHOOK_URL
    )

    if len(sys.argv) == 2 and sys.argv[1] == "update_chats":
        update_chats()
        updater.stop()
    else:
        while True:
            check_subscriptions()
            time.sleep(60)
